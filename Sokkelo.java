import java.util.Scanner;

class MazeGenerator {
	
	
	public char[][] generateEasyMaze() {
		char[][] board = {{ '_', '_', '_', '_', '_', '≈', '_' },
				{ '|', ' ', '|', ' ', '|' , ' ',  '|' },
				{'|', ' ', ' ', ' ', ' ', ' ', '|'},
				{'|', ' ', '|', ' ', '|', ' ',  '|'},
				{'|', ' ', ' ', ' ', ' ', ' ', '|'},
				{' ', '¯', '¯', '¯', '¯', '¯', ' '}};
		return board;
	}
	
	public char[][] generateMediumMaze() {
		char[][] board = {{ ' ', '_', '_', '_', '_', '_', '_', '_', '_' },
				{ '|', ' ', '|', ' ', ' ' , ' ', '|', ' ', '|' },
				{'|', ' ', ' ', ' ', '|', ' ', ' ', ' ', '|'},
				{'|', '—', '—', '—', '|', ' ', ' ', '—', '|'},
				{'|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|'},
				{'|', ' ', '|', '¯', '|', ' ', ' ', '—', '|'},
				{'|', ' ', '|', ' ', '|', '—', ' ', ' ', '|'},
				{'|', ' ', '|', ' ', ' ', ' ', ' ', ' ', '|'},
				{'|', ' ', '|', '—', '—', '—', '—', '—', '|'},
				{'|', ' ', '|', ' ', ' ', ' ', ' ', ' ', '|'},
				{'|', ' ', ' ', ' ', '|', '¯', '¯', '≈', '|'},
				{' ', '¯', '¯', '¯', ' ', '¯', '¯', ' ', ' '}};
		return board;
	}
	
	public char[][] generateHardMaze() {
		char[][] board = {{'|', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_',  '|'},
				{ '|', ' ', '|', ' ', ' ' , ' ', ' ', ' ', ' ', ' ', '|', ' ', ' ', '|' },
				{ '|', ' ', '|', '—', ' ' , ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|' },
				{ '|', ' ', ' ', ' ', ' ' , '|', ' ', '|', ' ', ' ', '|', ' ', ' ', '|' },
				{ '|', '—', '|', ' ', '|' , ' ', ' ', '|', ' ', ' ', ' ', ' ', '—', '|' },
				{ '|', ' ', ' ', ' ', '|' , ' ', '|', '|', ' ', '_', ' ', '|', ' ', '|' },
				{ '|', ' ', '|', '—', ' ' , ' ', '|', '_', ' ', ' ', ' ', '|', '|', '|' },
				{ '|', '—', '|', ' ', ' ' , ' ', '|', 'o', '|', ' ', '—', '—', ' ', '|' },
				{ '|', ' ', '|', ' ', '|' , ' ', '|', ' ', ' ', ' ', '|', ' ', ' ', '|' },
				{ '|', ' ', '|', ' ', '|' , '—', '|', ' ', '|', ' ', ' ', ' ', ' ', '|' },
				{ '|', ' ', '|', ' ', '|' , ' ', ' ', ' ', ' ', '—', '|', '—', '—', '|' },
				{ '|', ' ', ' ', ' ', '|' , ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|' },
				{'|', '≈', '¯', '|', '¯', '¯', '¯', '¯', '¯', '¯', '¯', '¯', '¯',  '|'}};
		return board;
	}
}

class Game {
	
	char[][] board;
	MazeGenerator easyMaze = new MazeGenerator();
	MazeGenerator mediumMaze = new MazeGenerator();
	MazeGenerator hardMaze = new MazeGenerator();
	Scanner userinput = new Scanner(System.in);
	Position position = new Position(0, 0);
	int pos1;
	int pos2; 
	
	
	//Methods
	public static void printMaze(char[][] board) {
		for(char[] row : board) {
			for(char c : row) {
				System.out.print(c);
			}
			System.out.println();
		}
	}

	public void startGame() {
        System.out.println("Welcome to Tomi's Maze Game!\nPlease choose one of the following options:\n(S)Start\n(H)Help\n(Q)Quit\n(I)Info");
        boolean stop = false;
        while(!stop)
        {
        	String action = userinput.nextLine().toUpperCase();
            switch(action) 
            {
                case "S":
                    System.out.println("Choose one of the following levels: \n1. Easy\n2. Medium\n3. Hard\nQ. Quit\nH. Help");
                    stop = true;
                    lvl();
    				break;
                case "H":
                    System.out.println("Find your way out of the maze!\nMove with WASD keys to move player location.\nPlayer location is marked as 'o'\nYou can't hit the walls!\nGood luck\n");
                    System.out.println("Please choose new action:\n(S)Start\n(H)Help\n(Q)Quit\n(I)Info");
                    break;
                case "Q":
                    System.out.println("Quitting...");
                    stop = true;
                    break;
                case "I":
                    System.out.println("Hi! This is Java based console game made by Tomi Heikkonen in the end of 2020.\nHope you enjoy and you can contact me throught email: heikkonen.tomi@gmail.com");
                    System.out.println("Please choose new action:\n(S)Start\n(H)Help\n(Q)Quit\n(I)Info");
                    break;
                default:
                    System.out.println("Invalid command! Please try again!");
                    break;
                    
            }
            
        }   
	}
	
	public void lvl() {
		boolean end = false;
		while(!end) {
			String choice = userinput.nextLine().toUpperCase();
			switch(choice) {
			case "1":
				this.board = easyMaze.generateEasyMaze();
				position = new Position(4, 3);
				end = true;
				movement();
				break;
			case "2":
				this.board = mediumMaze.generateMediumMaze();
				position = new Position(1, 1);
				movement();	
				end = true;
				break;
			case "3":
				this.board = hardMaze.generateHardMaze();
				position = new Position(7, 7);
				movement();
				end = true;
				break;
			case "H":
                System.out.println("Find your way out of the maze!\nMove with WASD keys to move player location.\nPlayer location is marked as 'o'\nIf you hit the wall you will be respawned back to a beginning!\nGood luck\n");
                System.out.println("Choose one of the following levels: \n1. Easy\n2. Medium\n3. Hard\nQ. Quit\nH. Help");
                break;
			case "Q":
				System.out.println("Quitting...");
				end = true;
				break;
			default:
				System.out.println("Invalid command. Please try again!");
				break;
			}

		}
	}
    
    public void movement()
    {
    	boolean quit = false;
        while(!quit) {
            board[position.y][position.x] = 'o';
            printMaze(board);
            pos1 = position.y;
            pos2 = position.x;
            char c = ' ';
            String moveTo = userinput.nextLine().toUpperCase();
            String menu;
			switch (moveTo) {
				case "W":
					if(c==(board[position.y-1][position.x]))
					{
						position.y--;
					}
					else if(board[position.y-1][position.x] == '≈') {
						position.y--;
						board[position.y][position.x] = 'o';
						board[pos1][pos2] = ' ';
						printMaze(board);
						System.out.println("You WIN!");
						System.out.println("Enter M to get back to Main Menu. Press anything else to Quit!");
						menu = userinput.nextLine().toUpperCase();
						if(menu.equals("M")) {
							startGame();
						}
						else {
							System.out.println("Quitting...");
							quit = true;
							break;
						}
					}
					else if(c!=(board[position.y-1][position.x])){
	        		    System.out.println("You hit the wall!");
	       			}
					break;					
				case "A":
					if(c==(board[position.y][position.x-1]))
					{
						position.x--;		
					}
					else if(board[position.y][position.x-1] == '≈') {
						position.x--;
						board[position.y][position.x] = 'o';
						board[pos1][pos2] = ' ';
						printMaze(board);
						System.out.println("You WIN!");
						System.out.println("Enter M to get back to Main Menu. Press anything else to Quit!");
						menu = userinput.nextLine().toUpperCase();
						if(menu.equals("M")) {
							startGame();
						}
						else {
							System.out.println("Quitting...");
							quit = true;
							break;
						}
					}
					else if(c!=(board[position.y][position.x-1])) {
						System.out.println("You hit the wall!");					
					}
					break;
				case "S":
					if(c==(board[position.y+1][position.x]))
					{
						position.y++;
					}
					else if(board[position.y+1][position.x] == '≈') {
						position.y++;
						board[position.y][position.x] = 'o';
						board[pos1][pos2] = ' ';
						printMaze(board);
						System.out.println("You WIN!");
						System.out.println("Enter M to get back to Main Menu. Press anything else to Quit!");
						menu = userinput.nextLine().toUpperCase();
						if(menu.equals("M")) {
							startGame();
						}
						else {
							System.out.println("Quitting...");
							quit = true;
							break;
						}
					}
					else if(c!=(board[position.y+1][position.x])){
						System.out.println("You hit the wall!");				
					}
					break;
				case "D":
					if(c==(board[position.y][position.x+1]))
					{
						position.x++;
					}
					else if(board[position.y][position.x+1] == '≈') {
						position.x++;
						board[position.y][position.x] = 'o';
						board[pos1][pos2] = ' ';
						printMaze(board);
						System.out.println("You WIN!");
						System.out.println("Enter M to get back to Main Menu. Press anything else to Quit!");
						menu = userinput.nextLine().toUpperCase();
						if(menu.equals("M")) {
							startGame();
						}
						else {
							System.out.println("Quitting...");
							quit = true;
							break;
						}
					}
					else if(c!=(board[position.y][position.x+1])) {
						System.out.println("You hit the wall!");					
					}
					break;
				case "Q":
					System.out.println("Quitting...");
					quit = true;
					break;
				default:
					System.out.println("Invalid command!");
					System.out.println("Please try again!");
					break;
					} 
				board[pos1][pos2] = ' ';
			}
    }	
}

class Position {
	int y;
	int x;
	
	Position(int y, int x)
	{
		this.y = y;
		this.x = x;
	}
}

public class Sokkelo {
	
	static Game game = new Game();

	public static void main(String[] args) {
		game.startGame();

	}
}
   